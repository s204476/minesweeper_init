import javafx.application.Application;
import javafx.event.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.*;
import javafx.scene.input.*;
import javafx.scene.control.Label;
import javafx.geometry.Pos;
import javafx.scene.canvas.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class NumberInput {
	VBox vbox;
	TextField textField;
	Stage stage;
	String value;
	
	public VBox display() {
		textField = new TextField();
		vbox= new VBox();
		vbox.getChildren().add(textField);
		value = textField.getText();
		textField.textProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, 
		        String newValue) {
		        if (!newValue.matches("\\d*")) {
		        	System.out.println(value);
		            textField.setText(newValue.replaceAll("[^\\d]", ""));
		            value = textField.getText();
		        }
		    }
		});

		return vbox;
	}
}
