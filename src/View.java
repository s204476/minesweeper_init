import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.event.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.shape.*;
import javafx.scene.input.*;
import javafx.scene.control.TextField;
import javafx.geometry.Pos;
import javafx.scene.canvas.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class View extends Application{
	private int unit = 30;
	private int height = 750;
	private int width = 450;
	
	private Pane rootPane;
	private Model model;
	private int totalMines = 30;
	
	private Stage stage;
	private Scene sceneOne;
	private Scene sceneTwo;
	private VBox vbox;
	TextField textField;
	public static void main(String[] args) {
		launch(args);
	}
	
	
	private Scene createSceneOne() {
		Button btn = new Button("New game");
		btn.setOnAction(e-> switchScene(sceneTwo));
		Button btn2 = new Button("Resume game");
		vbox = new VBox(btn, btn2);
		Scene sceneOne = new Scene(vbox, 500, 500);
		
		return sceneOne;
	}
	
	private void switchScene(Scene scene) {
		stage.setScene(scene);
	}
	
	private Scene createSceneTwo() {
		rootPane = new Pane();
		 model = new Model(height/unit, width/unit, totalMines, unit);
		 rootPane.setPrefSize(height, width);
		 model.init();
		 
		 for(int i =0; i<model.getGrid().length; i++) {
			 for(int j=0; j<model.getGrid()[0].length; j++) {
				 Tile tile = model.getGrid()[i][j];

				 tile.setTranslateX(i*unit);
				 tile.setTranslateY(j*unit);
				 
				 model.getGrid()[i][j].setOnMouseClicked(e->handeTilelClick(e, tile));

				 rootPane.getChildren().add(tile);
			 }
		 }
		 sceneTwo = new Scene(rootPane);
		return sceneTwo;
	}

	public void start (Stage topLevelStage) {
		sceneOne=createSceneOne();
		sceneTwo=createSceneTwo();
		stage=topLevelStage;
		stage.setScene(createSceneOne());
		topLevelStage.show();
	}
	
	public void handeTilelClick(MouseEvent e, Tile tile) {
		if(e.getButton()== MouseButton.PRIMARY) {
        	model.revealTile(tile);
        }else if(e.getButton()== MouseButton.SECONDARY) {
        	model.addFlag(tile);
        }
	}
}
