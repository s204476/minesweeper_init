import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.FontWeight;
import javafx.scene.effect.DropShadow; 


class Tile extends StackPane{
	private Rectangle square;
	private Text text; 
	
	public boolean mined = false;
	public boolean revealed=false;
	public boolean flaged = false;
	public int totalMines;
	public int x;
	public int y;
	
	Color unrevealedColor = Color.rgb(102, 102, 102);
	Color revealedColor = Color.rgb(189, 189, 189);

	public Tile(int unit, int _x, int _y) {
		x=_x;
		y=_y;
		square = new Rectangle(unit, unit);
		text = new Text();
		text.setFont(Font.font("Tomaha", FontWeight.EXTRA_BOLD, 15));
		square.setFill(unrevealedColor);

        square.setEffect(new DropShadow(3.0, Color.BLACK));
		square.setStroke(Color.WHITE);
		

		getChildren().addAll(square, text);
		
		setOnMouseClicked(event->{

		});
	}
	
	public void reveal() {
		revealed=true;
		square.setFill(revealedColor);

		if(mined) {
			text.setText("X");
		}else {		
			if(totalMines != 0) {
				text.setText(String.valueOf(totalMines));				
			}
		}

	}
	public boolean isRevealed() {
		return revealed;
	}
	public boolean isMined() {
		return mined;
	}
	public void addFlag() {
		if(!revealed) {
			flaged=!flaged;
			if(flaged) {
				square.setFill(Color.rgb(153, 0, 0));					
			}else {
				square.setFill(revealedColor);					
			}			
		}
	}
	
	public void addMine() {
		mined=true;
	} 
	public void setTotalMines(int n) {
		totalMines=n;
	}
	public String toString() {
		return "Tile";
	}
}
