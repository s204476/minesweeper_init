import java.util.*;

public class Model {
	public Tile[][] grid;
	int rows;
	int cols;
	public int totalMines;
	public int unit;
	// TODO: check to duplicate mined tiles
	public Model(int _rows, int _cols, int _totalMines, int _unit) {
		grid = new Tile[_rows][_cols];
		unit = _unit;
		rows = _rows;
		cols = _cols;
		totalMines = _totalMines;
	}
	
	public void init() { 
		for (int i =0; i<rows; i++) {
			for (int j =0; j<cols; j++) {

				grid[i][j] = new Tile(unit, i, j);
			}
		}
		randomFill();
		for (int i =0; i<rows; i++) {
			for (int j =0; j<cols; j++) {
				grid[i][j].setTotalMines(countNeighbors(i,j));
			}
		}
	}

	public int countNeighbors( int i, int j) {
	    int total = 0;
	    if(grid[i][j].isMined()) {
	    	
	    	total =-1;
	    	return total;
	    } 
	    
	    for (int xoff = -1; xoff <= 1; xoff++) {
	      int celli = i + xoff;
	      if (celli < 0 || celli >= rows) continue;
	  
	      for (int yoff = -1; yoff <= 1; yoff++) {
	        int cellj = j + yoff;
	        if (cellj < 0 || cellj >= cols) continue;
	  
	        Tile neighbor = grid[celli][cellj];

	        if (neighbor.isMined()) {
	          total++;
	        }
	      }
	    }
	    return total;
	}

	public void randomFill () {
		int totalAdded = 0;
		int randX;
		int randY;
		
		while(totalAdded != totalMines) {
			randX =(int) (Math.random()* (rows));
			randY =(int) (Math.random()* (cols));
			
			grid[randX][randY].addMine();
			totalAdded++;
		}
	}
	
	public void addFlag(Tile tile) {
		tile.addFlag();
	}
	public void revealTile(Tile tile) {
	    tile.reveal();
	    if (tile.totalMines== 0) {
	      floodFill(tile);
	    }
	}
	
	public void floodFill(Tile tile) {
		 for (int xoff = -1; xoff <= 1; xoff++) {
		      int celli = tile.x + xoff;
		      if (celli < 0 || celli >= rows) continue;
		  
		      for (int yoff = -1; yoff <= 1; yoff++) {
		        int cellj = tile.y + yoff;
		        if (cellj < 0 || cellj >= cols) continue;
		  
		        Tile neighbor = grid[celli][cellj];

		        if (!neighbor.revealed) {
		        	revealTile(neighbor);
		        }
		      }
		    }
	}
	public Tile[][] getGrid() {
		return grid;
	}
}
